﻿using BnSFoVChanger.Common;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Linq;

namespace BnSFoVChanger.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        private bool _isGameClosed;
        private bool _isGameRunning;
        private int _defaultValue;
        private string _configFilepath;
        private string _ZoomValue;

        private string Nation;

        private DelegateCommand _StartGame;
        private DelegateCommand _setDefaultValue;
        private DelegateCommand _applyValue;
        private DelegateCommand _selectFolder;
        private DelegateCommand _selectGame;

        public MainWindowViewModel()
        {
            ReadDefaultPath();
            CheckGameContinously();
        }
        
        public bool IsGameClosed { get => _isGameClosed; set => SetField(ref _isGameClosed, value); }
        public bool IsGameRunning { get => _isGameRunning; set => SetField(ref _isGameRunning, value); }
        public int DefaultValue { get => _defaultValue; set => SetField(ref _defaultValue, value); }
        public string ConfigFilepath { get; set; } = "";
        public string ZoomValue { get => _ZoomValue; set => SetField(ref _ZoomValue, value); }

        public ICommand SetDefaultValue { get => _setDefaultValue ?? new DelegateCommand((o) => iSetDefaultValue()); }
        public ICommand ApplyValue { get => _applyValue ?? new DelegateCommand((o) => ApplyConfiguration(ZoomValue)); }
        public ICommand SelectConfigFile { get => _selectFolder ?? new DelegateCommand((o) => WriteConfigPath()); }
        public ICommand SelectGame { get => _selectGame ?? new DelegateCommand((o) => SelectGameMethod()); }
        public ICommand StartGame { get => _StartGame ?? new DelegateCommand((o) => StartGameMethod()); }

        public void iSetDefaultValue()
        {
            int defaultvalue = 70;
            ZoomValue = defaultvalue.ToString();
        }

        public void ApplyConfiguration(string input)
        {
            XDocument xdoc = XDocument.Load(GetConfigFile());
            
            xdoc.Root.Elements("option").Where(x => x.Attribute("name").Value == "maxZoom").Select(x => x.Attribute("value").Value = input).FirstOrDefault();
            xdoc.Save(GetConfigFile());
        }

        public string GetConfigFile()
        {
            string PATH = Environment.ExpandEnvironmentVariables(@"C:%HOMEPATH%\documents\BnS\");
            string configFile = @"\ClientConfiguration.xml";

            if (Directory.Exists(PATH))
            {
                List<string> subs = Directory.GetDirectories(PATH, "NC*").ToList();
                this.Nation = new DirectoryInfo(subs[0]).Name.ToLowerInvariant();
                char[] tmp = this.Nation.ToCharArray();

                for (int i = 0; i < 3; i++)
                    tmp[i] = char.ToUpper(tmp[i]);

                this.Nation = string.Join("", tmp);

                return subs[0] + configFile;
            }
            else
            {
                MessageBox.Show("Game wasn't start jet. Please start game first.");
                Process process = Process.GetProcessById(Process.GetCurrentProcess().Id);
                process.Kill();
                return string.Empty;
            }
        }

        public static void InsertLineInFile(string path, string line, int position)
        {
            List<string> lines = File.ReadAllLines(path).ToList();
            using (StreamWriter writer = new StreamWriter(path))
            {
                if (lines.Count > 0)
                {
                    for (int i = 0; i < position - 1; i++)
                    {
                        writer.WriteLine(lines[i]);
                    }
                }
                else
                {
                    for (int i = 0; i < position - 1; i++)
                    {
                        writer.WriteLine(string.Empty);
                    }
                }

                writer.WriteLine(line);

                for (int i = position; i < lines.Count; i++)
                {
                    writer.WriteLine(lines[i]);
                }
            }
        }

        public string ReadConfiguration(string path)
        {
            XDocument xdoc = XDocument.Load(path);

            var value = xdoc.Root.Elements("option").Where(x => x.Attribute("name").Value == "maxZoom").Select(x => x.Attribute("value").Value).FirstOrDefault();

            if (value != null)
            {
                ZoomValue = value;
                return value;
            }
            else
            {
                return "Please change your file path!";
            }
        }

        public void ReadDefaultPath()
        {
            if (!File.Exists("config.txt"))
            {
                File.Create("config.txt");
                //RestartApp();
            }

            WriteConfigPath();
        }

        public void RestartApp()
        {
            try
            {
                Process process = Process.GetProcessById(Process.GetCurrentProcess().Id);
                Process.Start(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, ""); process.Kill();
            }
            catch (ArgumentException ex)
            {
            }
        }

        public void SelectGameMethod()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = ".ink";
            ofd.DereferenceLinks = false;
            ofd.FileName = "Blade & Soul";
            ofd.Multiselect = false;

            Nullable<bool> result = ofd.ShowDialog();

            if (result == true)
                WriteGamePath(ofd.FileName);
        }

        public void StartGameMethod()
        {
            if (File.Exists("config.txt"))
            {
                List<string> txt = File.ReadAllLines("config.txt").ToList();

                if (txt.Count > 1 && txt[1] != string.Empty)
                {
                    Process.Start(txt[1]);
                }
                else
                {
                    MessageBox.Show("Path not set");
                }
            }
            else
            {
                MessageBox.Show("Config-File is missing!");
            }
        }

        public void WriteConfigPath()
        {
            string configFile = "config.txt";
            InsertLineInFile(configFile, GetConfigFile(), 1);

            ReadConfiguration(GetConfigFile());
        }

        public void WriteGamePath(string path)
        {
            string configFile = "config.txt";
            InsertLineInFile(configFile, path, 2);
        }




        public async void CheckGameContinously()
        {
            while (true)
            {
                IsGameRunning = await IsGameRunningAsync("Client.exe");
                await Task.Delay(1000);
            }
        }

        public async Task<bool> IsGameRunningAsync(string processName)
        {
            bool result = await Task.Run(() => Process.GetProcessesByName(processName.Replace(".exe", string.Empty)).Length > 0);

            return result;
        }
    }
}