﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BnSFoVChanger
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slide = sender as Slider;
            if (this.txtValue != null)
                this.txtValue.Text = Convert.ToInt32(slide.Value).ToString();
        }

        private void txtValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox Box = sender as TextBox;
            this.SliderValue.Value = Convert.ToInt64(Box.Text);
        }
    }
}
